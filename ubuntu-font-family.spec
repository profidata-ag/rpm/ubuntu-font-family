%global fontconf 81-ubuntu.conf

Name:           ubuntu-font-family
Version:        0.83
Release:        1%{?dist}
Summary:        Ubuntu font family

License:        Ubuntu Font Licence 1.0
URL:            https://design.ubuntu.com/font/
Source0:        https://assets.ubuntu.com/v1/0cef8205-%{name}-%{version}.zip
Source1:        %{fontconf}

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
Ubuntu font

%prep
%autosetup
chmod 644 LICENCE.txt

%build
# nothing to do

%install
rm -rf %{buildroot}
install -dm755 %{buildroot}%{_fontconfig_confdir}
install -dm755 %{buildroot}%{_fontbasedir}/ubuntu
install -Dm644 -t %{buildroot}%{_fontbasedir}/ubuntu *.ttf
install -Dm644 %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%files
%license LICENCE.txt
%{_fontconfig_confdir}/%{fontconf}
%{_fontconfig_templatedir}/%{fontconf}
%{_fontbasedir}/ubuntu/

%changelog
* Sat Jul  3 2021 Levent Demirörs <levent.demiroers@profidata.com> - 0.83-1
- Update to latest version 0cef8205
